'''
@Copyright: © 2021 CornerStone Robotics
@Author: Guangli
@Date: 2021-12-21 02:05:21
@LastEditTime: 2021-12-21 22:33:58
@LastEditors: Guangli
@Description: 1. print out desired OT data; 2. save to csv; 3. calculate data 
@FilePath: /PyOptiTrack_V1/OT_utility.py
'''
import numpy as np
import time
# from pyOptiTrack import PyOptitrack as PyOT
from NatNetClient import NatNetClient
# import MoCapData

# append data to a limitation length list
def buff_append( a, b, buff_size ):
    a.append(b) if len(a)<buff_size else a.pop(0)

def distance(pos1,pos2):
    pos1 = np.array(pos1)
    pos2 = np.array(pos2)
    return np.linalg.norm(pos1-pos2)

def decode_marker_id(id_num):
    model_id, marker_id = NatNetClient.decode_marker_id(id_num)
    return model_id,marker_id

#trace decorate function
# you can set variable CLOSE here to close the trace print function defined below
def traceFunc(CLOSE):
    def func_outer(trace_function):
        def func_inner(*args, **kwargs):
            if CLOSE == 1: # means not print
                return
            else:
                trace_function(*args, **kwargs)
        return func_inner
    return func_outer

@traceFunc(0)
def trace_arg(*args):
    print("".join(map(str,args)))
    #pass

@traceFunc(0)
def trace_time(clocktime, timecode, timecode_sub ):
    # print(trace_time.__code__.co_varnames)
    # print(locals().keys())
    out = ""
    out += "timestamp: " + str(clocktime)
    out += "\ntimecode: " + str(timecode)
    out += "\ntimecode_sub: " + str(timecode_sub)
    print( out )

@traceFunc(0)
def trace_marker( marker ):
    # marker_count = len(marker_list)
    out_str=""
    # out_str += "Labeled Marker Count:%3.1d\n"%( marker_count )
    # for i in range( 0, marker_count ):
    # out_str += "%sLabeled Marker %3.1d\n"%(" ",i)
    out_tab_str = " "
    # marker = marker_list[i]
    # out_str += labeled_marker.get_as_string(tab_str, level+2)
    tmp_id = marker.id_num
    model_id, marker_id = decode_marker_id(tmp_id)
    # out_str = ""
    out_str += "%sID                 : [MarkerID: %3.1d] [ModelID: %3.1d]\n"%(out_tab_str, marker_id,model_id)
    out_str += "%spos                : [%3.3f, %3.3f, %3.3f]\n"%(out_tab_str, marker.pos[0],marker.pos[1],marker.pos[2])
    out_str += "%ssize               : [%3.3f]\n"%(out_tab_str, marker.size)
    out_str += "%serr                : [%3.3f]"%(out_tab_str, marker.residual)
    print(out_str)
    # return out_str 

@traceFunc(1)
def trace_frame_description(frame_data):
    data_dict = frame_data
    order_list=[ "frameNumber", "markerSetCount", "unlabeledMarkersCount", "rigidBodyCount", "skeletonCount",
                "labeledMarkerCount", "timecode", "timecodeSub", "timestamp", "isRecording", "trackedModelsChanged" ]
    out_string = "    "
    for key in data_dict:
        out_string += key + "="
        if key in data_dict :
            out_string += str(data_dict[key]) + " "
        out_string+="/"
    trace_arg(out_string)

def print_rb(rb):
    trace_arg("rigid body streaming ID: ", rb.id_num, "\n pos:", rb.pos, "\n rot(x,y,z,w):", rb.rot, "\n error:", rb.error )

@traceFunc(0)
def trace_rb(rb):
    marker_list = rb.rb_marker_list
    print_rb(rb)
    print("Containing markers: %d" %(len(marker_list)) )
    print("***")    
    for marker in marker_list:
        # tmp_id = marker.id_num
        # model_id, marker_id, = self.decode_marker_data(marker)
        trace_marker(marker)
        print("----")
    print("***")

@traceFunc(1)
def print_configuration(natnetClient):
    print("Connection Configuration:")
    print("  Client:          %s"% natnetClient.local_ip_address)
    print("  Server:          %s"% natnetClient.server_ip_address)
    print("  Command Port:    %d"% natnetClient.command_port)
    print("  Data Port:       %d"% natnetClient.data_port)

    if natnetClient.use_multicast:
        print("  Using Multicast")
        print("  Multicast Group: %s"% natnetClient.multicast_address)
    else:
        print("  Using Unicast")

    #NatNet Server Info
    application_name = natnetClient.get_application_name()
    nat_net_requested_version = natnetClient.get_nat_net_requested_version()
    nat_net_version_server = natnetClient.get_nat_net_version_server()
    server_version = natnetClient.get_server_version()

    print("  NatNet Server Info")
    print("    Application Name %s" %(application_name))
    print("    NatNetVersion  %d %d %d %d"% (nat_net_version_server[0], nat_net_version_server[1], nat_net_version_server[2], nat_net_version_server[3]))
    print("    ServerVersion  %d %d %d %d"% (server_version[0], server_version[1], server_version[2], server_version[3]))
    print("  NatNet Bitstream Requested")
    print("    NatNetVersion  %d %d %d %d"% (nat_net_requested_version[0], nat_net_requested_version[1],\
    nat_net_requested_version[2], nat_net_requested_version[3]))

def trim_time_list(t_list):
    return [ t-t_list[0] for t in t_list ]

def keep_start_end_points(pos_list, t_list):
    start = 0
    end = 0
    threshold_1 = 0.00025
    threshold_2 = 0.0001
    for i in range(len(pos_list)-1):
        if abs(pos_list[i+1][0] - pos_list[i][0]) > threshold_1 or abs(pos_list[i+1][1] - pos_list[i][1]) > threshold_1 or abs(pos_list[i+1][2] - pos_list[i][2]) > threshold_1 :
            start = i
            break
    end = start
    for i in range( len(pos_list)-2, start, -1):
        if abs(pos_list[i+1][0] - pos_list[i][0]) > threshold_2 or abs(pos_list[i+1][1] - pos_list[i][1]) > threshold_2 or abs(pos_list[i+1][2] - pos_list[i][2]) > threshold_2 :
            end = i
            break
    print("start id: %d, end: %d; start time: %.3f, end time: %.3f; interval: %.3f" %(start,end, t_list[start], t_list[end], t_list[end]-t_list[start]))
    return start, end

def draw_list_pos(name, pos_list, time_list):
    import matplotlib.pyplot as plt
    fig = plt.figure(tight_layout=True)
    plt.title(name)
    lb = ["x", "y", "z"]
    for i in range(3):
        xt = time_list
        yt = [ pos[i] for pos in pos_list ]
        plt.plot(xt,yt,'--', label = lb[i])
    plt.xticks(np.linspace(0, time_list[-1],len(time_list)))
    plt.legend()
    plt.show()

def ot_streaming_simulation():
    import MoCapData
    frame_num = 4
    rb1_pos_list = []
    rb1_ori_list = []
    marker4_list = []
    data_list = []
    time_list = []
    while frame_num< 10:
        # hex_id + pos + marker size(mm)
        marker1 = MoCapData.LabeledMarker(0x10001 , [10,21,31], 0.89 )
        # marker1.add_pos( 0x10001 , [10,21,31], 0.89 )
        marker2 = MoCapData.LabeledMarker(0x10002 , [11,22,32], 0.89)
        # marker2.add_pos( 0x10002 , [11,22,32], 0.89 )
        marker3 = MoCapData.LabeledMarker(0x10003 , [12,23,33], 0.89)
        # marker3.add_pos( 0x10003 , [12,23,33], 0.89 )
        marker4 = MoCapData.LabeledMarker(0x00020 , [1111,222,333], 1.2 )
        # marker4.add_pos( 0x00020 , [1111,222,333], 1.2 )

        rb1 = MoCapData.RigidBody(1,[11. + frame_num ,22. + frame_num ,32. + frame_num], [0.,0.,0.,1.])
        rb1.add_rigid_body_marker(marker1)
        rb1.add_rigid_body_marker(marker2)
        rb1.add_rigid_body_marker(marker3)
        
        trace_rb(rb1)
        trace_arg( "\nOther marker:")
        trace_marker(marker4)
        time_list.append(time.time())
        rb1_pos_list.append([ rb1.pos[0], rb1.pos[1], rb1.pos[2] ])
        rb1_ori_list.append( [ rb1.rot[0], rb1.rot[1], rb1.rot[2], rb1.rot[3] ]  )
        frame_num = frame_num + 1

    time_list = trim_time_list(time_list)
    draw_list_pos( "Rigid Body Pivot Trajectory", rb1_pos_list, time_list)

    return data_list

if __name__ == "__main__":
    data_list = ot_streaming_simulation() 
    # save2CSV("abc.csv", data_list)