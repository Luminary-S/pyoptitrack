'''
@Copyright: © 2021 CornerStone Robotics
@Author: Guangli
@Date: 2021-12-18 12:59:16
@LastEditTime: 2021-12-22 20:08:37
@LastEditors: Guangli
@Description: optiTrack API for getting data from streaming Motive 3.0
@FilePath: /PyOptiTrack_V1/pyOptiTrack.py
'''
# Uses the Python NatNetClient.py library to establish a connection (by creating a NatNetClient),
# and receive data via a NatNet connection and decode it using the NatNetClient library.

import sys, time, datetime
from NatNetClient import NatNetClient
from OT_utility import *
from Data2CSV import *

class PyOptitrack:

    def __init__(self):        
        self.mocap_buf = []
        self._BUFF_SIZE = 10
        self.rb_buf = [] 
        self.streaming_client = NatNetClient()
    
    def set_server_ip(self, server_ip):
        self.server_ip = server_ip
    def set_client_ip(self, client_ip):
        self.client_ip = client_ip 
    def print_config(self):
        self.streaming_client.print_configuration()
    def set_print_level(self, level = 0):
        self.streaming_client.set_print_level(level)
    
    def open(self):   
        is_running = self.streaming_client.run()
        return is_running
    def close(self):
        self.streaming_client.shutdown()
    def check_connected(self):
        if self.streaming_client.connected() is False:
            print("ERROR: Could not connect properly.  Check that Motive streaming is on.")
            try:
                sys.exit(2)
            except SystemExit:
                print("...")
            finally:
                print("exiting")
        return self.streaming_client.connected()

    #NOTE: should be register in init_streaming,
    # This is a callback function that gets connected to the NatNet client
    # and called once per mocap frame.
    def receive_new_frame(self, data_dict):
        self.mocap_buf.append(data_dict)
        trace_frame_description(data_dict)
        # buff_append( self.mocap_buf, data_dict["mocap_data"], self._BUFF_SIZE )
        # self.mocap_buf = self.buff_append( mocap_buf, data_dict, self._BUFF_SIZE )

    #NOTE: should be register in init_streaming, here not use
    # This is a callback function that gets connected to the NatNet client. It is called once per rigid body per frame
    def receive_rigid_body_frame( self, new_id, position, rotation, error ):
        rb_dict = {}
        rb_dict["id"] = new_id
        rb_dict["position"] = position
        rb_dict["rotation"] = rotation
        rb_dict["error"] = error
        rb_buf = self.rb_buf
        self.rb_buf = self.buff_append( rb_buf, rb_dict, self._BUFF_SIZE )

    def check_buff(self):
        return True if len(self.mocap_buf) > 0 else False

    def get_mocap_data(self):
        return self.mocap_buf[-1]

    def get_rigidbody_data(self, moCap, id):
        rb_list = moCap.rigid_body_data.rigid_body_list
        for rb in rb_list:
            if rb.id_num == id:
                return rb
            else:
                continue
        trace_arg("No rigidbody in id ", id, " found!")
        return None

    '''
    @function name: get_rb_marks_data
    @berief: get marker_list in a RB, return marker_list
    @note: 
    @param {*} rb_id : rigid body model id, can be checked in the Motive property
    @return {*} marker_list: [marker1, marker2...]; marker:  self.id_num, self.pos, self.size,  self.residual
    @example: 
    '''    
    def get_rb_marks_data(self, moCap, rb_id):
        # in marker set find model id == id and save in list
        marker_list = []
        all_marker_list = moCap.labeled_marker_data.labeled_marker_list
        for marker in all_marker_list:
            tmp_id = marker.id_num
            model_id, marker_id = decode_marker_id(tmp_id)
            if model_id == rb_id:
                marker_list.append(marker)
        # trace_mark(marker_list)
        return marker_list 
    
    @staticmethod
    def rb_add_marker_list(rb, marker_list):
        for i in marker_list:
            rb.add_rigid_body_marker(i) 
        return rb

    def get_marker_data(self, moCap, id):
        marker_list = moCap.labeled_marker_data.labeled_marker_list
        for marker in marker_list:
            tmp_id = marker.id_num
            model_id, marker_id = decode_marker_id(tmp_id)
            if marker_id == id:
                return marker
            else:
                continue
        trace_arg("No marker with id ", id, " found!")
        return None
        
    '''
    @function name: decode_rb_data
    @brief: decode rb's useful message
    @description: if marker in no label, still can be find in label_marker_data, but model_id = 0
    @param {*} rb : class  MoCapData.LabeledMarker
    @return {*} # pos:[x,y,z]; id_num contains information of model_id and marker_id, furthur in NatNetClient.__decode_marker_id
    '''    
    @staticmethod
    def decode_marker_data(marker):
        tmp_id = marker.id_num
        model_id, marker_id = decode_marker_id(tmp_id)
        return model_id, marker_id, marker.pos, marker.residual, marker.size

    '''
    @funticon name: decode_rb_data
    @berif: decode rb's useful message
    @description: more information of rb can be checked in the rb class 
    @param {*} rb : class  MoCapData.RigidBody
    @return {*} # pos:[x,y,z]; rot: [x,y,z,w]; 
    '''
    @staticmethod
    def decode_rb_data(rb):
        return rb.id_num, rb.pos, rb.rot, rb.error 
    
    @staticmethod
    def cal_mark_list_neighbour_distance(marker_list):
        d_list = []
        for i in range(len(marker_list)-1):
            d_list.append( distance( marker_list[i].pos, marker_list[i+1].pos ) )
        return d_list
    '''
    @funticon name: init_streaming
    @berif: init natnetclient setting, return a dict of client setting
    @note: callback function should be defined in the NatNetClient.py
    @param {*} client_ip, default is local to local 
    @param {*} server_ip, default is local to local 
    @return {*} optionsDict
    @example: 
    '''
    def init_streaming(self, client_ip, server_ip):
        optionsDict = {}
        optionsDict["clientAddress"] = client_ip
        optionsDict["serverAddress"] = server_ip
        optionsDict["use_multicast"] = True

        # streaming_client = NatNetClient()
        self.streaming_client.set_client_address(optionsDict["clientAddress"])
        self.streaming_client.set_server_address(optionsDict["serverAddress"])
        self.streaming_client.set_use_multicast(optionsDict["use_multicast"])

        # register callback function
        self.streaming_client.new_frame_listener = self.receive_new_frame
        # self.streaming_client.rigid_body_listener = self.receive_rigid_body_frame
        return optionsDict
        
    def streaming(self, server_ip, client_ip):
        self.init_streaming(server_ip, client_ip)
        is_running = self.open()
        if not is_running:
            print("ERROR: Could not start streaming client.")
            try:
                sys.exit(1)
            except SystemExit:
                print("...")
            finally:
                print("exiting")
    

def test_OT( server_ip = "127.0.0.1", client_ip = "127.0.0.1" ):
    OT = PyOptitrack() 
    OT.set_print_level(0) # print each nth frame, 0 means not print, frame data example can be checked in readme.md
    OT.streaming( server_ip, client_ip ) # if you want to set the port, pls check the motive software and revise in the NatNetClient.py __init__
    print_configuration(OT.streaming_client)
    is_looping = True
    time.sleep(0.5) # let streaming get data 
    rb1_pos_list = []
    rb_list = []
    marker4_list = []
    time_list = []
    frame_num = 0
    while is_looping and frame_num < 700 :
        frame_num += 1
        trace_arg("------------start loop ", str(frame_num), "----- ------------")
        if OT.check_buff(): # data buff is not empty
            ### 1. get motive latest capture frame data
            moCapframe = OT.get_mocap_data()
            timestamp = moCapframe["timestamp"] 
            print(datetime.datetime.fromtimestamp(timestamp))       
            # timecode = moCapframe[ "timecode"] 
            # timecode_sub = moCapframe[ "timecode_sub"] 
            # trace_time( datetime.datetime.fromtimestamp(timestamp), timecode, timecode_sub)

            moCap = moCapframe["mocap_data"]
            ### 2. RB data operation, get rb by id, id can be referred to [[OT_motive_streaming #P1]]
            rb = OT.get_rigidbody_data( moCap, 1 ) # returned rb doesn't contain its marker_list
            # decode rb's useful message, rot in [x,y,z,w] , pos in [x,y,z]mm
            rb.id_num, rb.pos, rb.rot, rb.error = OT.decode_rb_data(rb)             
            marker_list = OT.get_rb_marks_data( moCap, 1 ) # return rb's marker_list
            rb = OT.rb_add_marker_list( rb, marker_list )
            trace_rb( rb ) # print RB and its markers information
            trace_arg("RB markers distance(between in order): ", OT.cal_mark_list_neighbour_distance(marker_list))
            trace_arg("#####")

            ### 3. marker operation, get rb by id, refer to [[OT_motive_streaming #P2]] to know how to get id
            marker = OT.get_marker_data(moCap, 7) 
            # decode rb's useful message,  pos in [x,y,z]
            model_id, marker_id, pos, residual, size = OT.decode_marker_data(marker)
            trace_marker(marker)

            ### 4. save rb pos data into list for plot
            rb1_pos_list.append( [ rb.pos[0], rb.pos[1], rb.pos[2] ] )
            time_list.append( timestamp )

            ### 5. save rb data into list for csv store
            rb_list.append((rb,timestamp))
            marker4_list.append((marker,timestamp))
        trace_arg("------------end loop-----------------")
        # time.sleep(0.01) # control loop time
        
    time_list = trim_time_list(time_list)
    # 6. find the start and end point of a moving rigidbody, can be used for repeated experiment
    start_index , end_index = keep_start_end_points(rb1_pos_list,time_list)

    t = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
    # 7. save RB data into csv file
    rbData2csv(rb_list,t)
    # 8. save marker data into csv file
    markerData2csv(marker4_list,t)
    # 9. draw x,y,z trajectory of a position, here  is RB pivot point
    draw_list_pos( "Rigid Body Pivot Trajectory", rb1_pos_list, time_list)

if __name__ == "__main__":
    #in ubuntu    
    server_ip = "192.168.0.115"  # ubuntu setting
    client_ip = "192.168.0.117"   # windows setting, fixed 
    test_OT(server_ip, client_ip) # two pc test, comment below, and change two IPs.
    # test_OT()  # local test, comment above three
