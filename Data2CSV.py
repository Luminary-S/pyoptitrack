'''
@Copyright: © 2021 CornerStone Robotics
@Author: Guangli
@Date: 2021-12-21 13:52:49
@LastEditTime: 2021-12-21 22:30:30
@LastEditors: Guangli
@Description: save Optitrack data to csv file
@FilePath: /PyOptiTrack_V1/Data2CSV.py
'''
import csv,time
# from typing import ForwardRef
from OT_utility import *

FolderName = "./data/"

def save2CSV(filename, header_list, data_list ):    
    with open(filename, mode="w", encoding="utf-8") as f:
        csv_file = csv.writer(f)
        csv_file.writerow(header_list)
        for i in data_list:
            csv_file.writerow(i)

def CSV2Data(filename, data_list):
    pass

def rbData2csv(rb_list, filetime):
    #t:s; px:mm
    headers = ["t","id","px", "py","pz", "qx", "qy", "qz", "qw", "err"]
    data_list = []
    for rb,t in rb_list:
        data = rb2list(rb)
        data.insert(0,t)
        data_list.append(data)     
    fname = FolderName + "RB_mdID_" + str(data_list[0][1]) + "_" + str(filetime) + ".csv"
    save2CSV( fname, headers, data_list )

def rb2list(rb):    
    data = [ rb.id_num, rb.pos[0], rb.pos[1], rb.pos[2], rb.rot[0], rb.rot[1], rb.rot[2], rb.rot[3], rb.error ]
    return data

def markerData2csv(marker_list,filetime):
    headers = ["t","markID","modelID","px", "py","pz", "err"]
    data_list = []
    for mk,t in marker_list:
        data = marker2list(mk)
        data.insert(0,t)
        data_list.append(data) 
    # t = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime()) 
    fname = FolderName + "MK_mkID_" + str(data_list[0][1]) +"_mdID_" + str(data_list[0][2]) + "_" + str(filetime) + ".csv"
    save2CSV( fname, headers, data_list )

def marker2list(marker):
    tmp_id = marker.id_num
    model_id, marker_id = decode_marker_id(tmp_id)
    return [ marker_id,model_id, marker.pos[0], marker.pos[1], marker.pos[2], marker.residual ]
    # data = [marker.id_num, marker.pos[0], marker.pos[1], marker.pos[2], marker.error ]
    # return dict(zip(headers, data))

