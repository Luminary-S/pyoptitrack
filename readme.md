API library for data fetch from OptiTrack Motive 3.* data streaming (tested in windows and ubuntu simultaneously)
---

## requirements
```shell
#create req
pip install pipreqs -i https://pypi.tuna.tsinghua.edu.cn/simple  (https://pypi.mirrors.ustc.edu.cn/simple, http://pypi.douban.com/simple/ )
# only create reqs for directory usage
pipreqs ./ --encoding=utf8  
# export environment all package to file. 
pip freeze > requirements.txt 

#install as reqs
pip install -r requirement.txt
```
## description
1. [x] get the rigid body(RB) pivot point(PP) id + position + orientation + error
2. [x] get the RB markers list 
3. [x] get a single marker markerid + modelid + position + error
3. [x] calculate desired mark list/ RB PP list (in order) length list
4. [x] save RB data into csv file, save marker data into csv file
5. [x] calculate the start index and end index of a pose list in a movement clip, designing for the repeat movement experiment.
6. [x] plot pose x,y,z trajectory in 2D

## main use in ubuntu
1. open Motive 3.* data streaming, setting the streaming: 
`Enable: True; Local interface:192.168.0.117 ; Transmission type: unicast`
2. network setting in linux and windows:
```
    server_ip = "192.168.0.115"  # ubuntu setting
    client_ip = "192.168.0.117"   # windows setting, fixed 
```
3. python run `pyOpticTrack.py`, tested in python 3.* ubuntu 18.04, the loop data print out means OK.
4. `test_OT` is a sample for understanding the API.

## important function details
in `NatNetClient.py`
1. `print_level` can be set to print the mocap info, default is 0, can be revised by `set_print_level()`
2. `__process_message()` is the main data processing function, for data stream which has a head 4 byte data as `NAT_FRAMEOFDATA`  after decoding will get the mocap frame data and decode the byte data to information by `__unpack_mocap_data` function.

## moCap from the streaming raw data
```
data_dict={}
data_dict["frame_number"]=frame_number
data_dict[ "marker_set_count"] = marker_set_count                # not use
data_dict[ "unlabeled_markers_count"] = unlabeled_markers_count  # not use
data_dict[ "rigid_body_count"] = rigid_body_count
data_dict[ "skeleton_count"] =skeleton_count                     # not use
data_dict[ "labeled_marker_count"] = labeled_marker_count        # not use
data_dict[ "timecode"] = timecode
data_dict[ "timecode_sub"] = timecode_sub
data_dict[ "timestamp"] = timestamp
data_dict[ "is_recording"] = is_recording
data_dict[ "tracked_models_changed"] = tracked_models_changed    # not use
data_dict["mocap_data"] = mocap_data                             # all data
```

## frame data example print out
```
MoCap Frame Begin
-----------------
Frame #: 1188848
  Marker Set Count:  2
    Model Name : RigidBody 002
    Marker Count :  3
      Marker   0 pos : [0.13,0.01,0.08]
      Marker   1 pos : [0.20,0.02,0.12]
      Marker   2 pos : [0.08,0.01,0.17]
    Model Name : all
    Marker Count :  3
      Marker   0 pos : [0.13,0.01,0.08]
      Marker   1 pos : [0.20,0.02,0.12]
      Marker   2 pos : [0.08,0.01,0.17]
  Unlabeled Markers Count:  1
    Marker Count :  1
      Marker   0 pos : [0.12,0.02,0.24]
  Rigid Body Count:   2
    ID            :   0
    Position      : [0.00, 0.00, 0.00]
    Orientation   : [0.00, 0.00, 0.00, 1.00]
    Marker Error  : 0.00
    Tracking Valid: False
    ID            :   2
    Position      : [0.14, 0.01, 0.12]
    Orientation   : [-0.00, 0.02, 0.00, 1.00]
    Marker Error  : 0.00
    Tracking Valid: True
  Skeleton Count:   0
  Labeled Marker Count:  4
    Labeled Marker   0
      ID                 : [MarkerID:   1] [ModelID:   2]
      pos                : [0.13, 0.01, 0.08]
      size               : [0.01]
      occluded           : [  0]
      point_cloud_solved : [  1]
      model_solved       : [  0]
      err                : [0.00]
    Labeled Marker   1
      ID                 : [MarkerID:   2] [ModelID:   2]
      pos                : [0.20, 0.02, 0.12]
      size               : [0.01]
      occluded           : [  0]
      point_cloud_solved : [  1]
      model_solved       : [  0]
      err                : [0.00]
    Labeled Marker   2
      ID                 : [MarkerID:   3] [ModelID:   2]
      pos                : [0.08, 0.01, 0.17]
      size               : [0.01]
      occluded           : [  0]
      point_cloud_solved : [  1]
      model_solved       : [  0]
      err                : [0.00]
    Labeled Marker   3
      ID                 : [MarkerID:  26] [ModelID:   0]
      pos                : [0.12, 0.02, 0.24]
      size               : [0.01]
      occluded           : [  0]
      point_cloud_solved : [  1]
      model_solved       : [  0]
      err                : [0.00]
  Force Plate Count:   0
  Device Count:   0
  Timestamp : 23776.96
  Mid-exposure timestamp : 295409068665
  Camera data received timestamp : 295409099627
  Transmit timestamp : 295409105645
MoCap Frame End
----------------- 
```

